import React from 'react';
import { Text, StyleSheet, View, Button, TouchableOpacity } from 'react-native';

const HomeScreen = ({ navigation }) => {
  return (
    <View>
      <Text style={styles.text}>React-native course!</Text>
      <View 
          style={styles.button}>
        <Button
          onPress={() => navigation.navigate('Components')}
          title="Go to Components Demo"
        />
      </View>
      <View 
          style={styles.button}>
        <Button
          title="Go to List Demo"
          onPress={() => navigation.navigate('List')}
        />
      </View>
      <View
          style={styles.button}>
        <Button
          title="Go to Image Screen"
          onPress={() => navigation.navigate('Image')}
        />
      </View>
      <View 
          style={styles.button}>
        <Button
          title="Go to Counter Screen"
          onPress={() => navigation.navigate('Counter')}
        />
      </View>
      <View 
          style={styles.button}>
        <Button
          title="Go to Color Screen"
          onPress={() => navigation.navigate('Color')}
        />
      </View>
      <View 
          style={styles.button}>
        <Button
          title="Go to Square Screen"
          onPress={() => navigation.navigate('Square')}
        />
      </View>
      <View 
          style={styles.button}>
        <Button
          title="Go to Text Screen"
          onPress={() => navigation.navigate('Text')}
        />
      </View>
      <View 
          style={styles.button}>
        <Button
          title="Go to Box Screen"
          onPress={() => navigation.navigate('Box')}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 30
  },

  button: {
    margin: 8
  }
});

export default HomeScreen;
