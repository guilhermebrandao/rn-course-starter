import React from 'react';
import { Text, View, StyleSheet } from 'react-native';

const BoxScreen = () => {
    return (
        <View style={styles.viewStyle}>
            <Text style={styles.textOneStyle}>Child #1</Text>
            <Text style={styles.textTwoStyle}>Child #2</Text>
            <Text style={styles.textThreeStyle}>Child #3</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    viewStyle: {
        borderWidth: 3,
        borderColor: 'red',
        flexDirection: 'row',
        height: 200, 
        justifyContent: 'space-around'

    },
    textOneStyle: {
        borderWidth: 2,
        borderColor: 'black',
        margin: 10
    },
    textTwoStyle: {
        borderWidth: 2,
        borderColor: 'black',
        margin: 10,
        flex: 1
    },
    textThreeStyle: {
        borderWidth: 2,
        borderColor: 'black',
        margin: 10
    }
});

export default BoxScreen;